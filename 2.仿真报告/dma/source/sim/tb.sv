`timescale 1ns / 1ps

module tb;    
    reg ddrphy_clkin;
    reg ddr_init_done;
    reg cam_pclk;

    bit cmos_frame_vsync0;
    bit cmos_frame_valid0;
    bit [31:0] wr_data_camera0;
    bit [7:0] bufn_i0;

    bit cmos_frame_vsync1;
    bit cmos_frame_valid1;
    bit [31:0] wr_data_camera1;
    bit [7:0] bufn_i1;

   bit [31:0] axi_awaddr;
   bit axi_awuser_ap;
   bit [3:0] axi_awuser_id;
   bit [3:0] axi_awlen;
   bit axi_awready;
   bit axi_awvalid;
   bit [31:0] axi_wdata;
   bit [3:0] axi_wstrb;
   bit axi_wready;
   bit [3:0] axi_wusero_id;
   bit axi_wusero_last;

task automatic m0_send_video;
    for(int i =0; i<560; i=i+1) begin
        // bufn_i0 = (bufn_i0==3) ? 0 :  bufn_i0 + 1;
        cmos_frame_vsync0 = 1;
        repeat(3) @(posedge cam_pclk);
        cmos_frame_vsync0 = 0;
        cmos_frame_valid0 = 1;
        for(int j=0; j<960; j=j+1) begin
            @(posedge cam_pclk)
                wr_data_camera0 = {$random}%16'hffff;
        end
        @(posedge cam_pclk);
        cmos_frame_valid0 = 0;
        bufn_i0 = (bufn_i0==3) ? 0 :  bufn_i0 + 1;
    end
endtask //automatic

task automatic m1_send_video;
    for(int i =0; i<560; i=i+1) begin
        // bufn_i0 = (bufn_i0==3) ? 0 :  bufn_i0 + 1;
        cmos_frame_vsync1 = 1;
        repeat(3) @(posedge cam_pclk);
        cmos_frame_vsync1 = 0;
        cmos_frame_valid1 = 1;
        for(int j=0; j<960; j=j+1) begin
            @(posedge cam_pclk)
                wr_data_camera1 = {$random}%16'hffff;
        end
        @(posedge cam_pclk);
        cmos_frame_valid1 = 0;
        bufn_i1 = (bufn_i1==3) ? 0 :  bufn_i1 + 1;
    end
endtask //automatic

task automatic hand_shake;begin
   while(axi_awvalid==0) @(ddrphy_clkin);
   axi_awready <= 1;
   while(axi_awvalid==1) @(ddrphy_clkin);
   axi_awready <= 0;
   repeat(3) @(posedge ddrphy_clkin);
   axi_wready <= 1;
   repeat(16) @(posedge ddrphy_clkin);
end
endtask //automatic


always #5 ddrphy_clkin = ~ddrphy_clkin;
always #10 cam_pclk = ~cam_pclk;

initial begin
    ddrphy_clkin = 0;
    ddr_init_done = 0;
    cam_pclk = 0;
    #25 ddr_init_done = 1;
    #20
    fork
        m0_send_video();
        m1_send_video();
    join
end

initial begin
    //repeat(500) @(posedge cam_pclk);
    while(1) hand_shake();
end

dma_test_top u_dma_test_top(
    .ddrphy_clkin(ddrphy_clkin),
    .ddr_init_done(ddr_init_done),
    .cam_pclk(cam_pclk),
//    master0    
    .cmos_frame_vsync0(cmos_frame_vsync0),
    .cmos_frame_valid0(cmos_frame_valid0),
    .wr_data_camera0(wr_data_camera0),
    .bufn_i0(bufn_i0),
//    master1
    .cmos_frame_vsync1(cmos_frame_vsync1),
    .cmos_frame_valid1(cmos_frame_valid1),
    .wr_data_camera1(wr_data_camera1),
    .bufn_i1(bufn_i1),
//    master2    
    .cmos_frame_vsync2(),
    .cmos_frame_valid2(),
    .wr_data_camera2(),
    .bufn_i2(),
//    master3    
    .cmos_frame_vsync3(),
    .cmos_frame_valid3(),
    .wr_data_camera3(),
    .bufn_i3(),

    .axi_awaddr(axi_awaddr),    
    .axi_awuser_ap(axi_awuser_ap), 
    .axi_awuser_id(axi_awuser_id), 
    .axi_awlen(axi_awlen),     
    .axi_awready(axi_awready),   
    .axi_awvalid(axi_awvalid),   
               
    .axi_wdata(axi_wdata),
    .axi_wstrb(axi_wstrb),     
    .axi_wready(axi_wready),    
    .axi_wusero_id(axi_wusero_id), 
    .axi_wusero_last(axi_wusero_last),
               
    .axi_araddr(),    
    .axi_aruser_ap(), 
    .axi_aruser_id(), 
    .axi_arlen(),     
    .axi_arready(),   
    .axi_arvalid(),   
               
    .axi_rdata(),     
    .axi_rid(),       
    .axi_rlast(),     
    .axi_rvalid()     
);

    reg grs_n;

GTP_GRS GRS_INST(
    .GRS_N (grs_n)
);

initial begin
    grs_n = 1'b0;
    #50 grs_n = 1'b1;
end

endmodule