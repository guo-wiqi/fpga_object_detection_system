module vsdma#(
    parameter ENABLE_READ = 1'b1,
    parameter W_BASEADDR = 32'd0,
    parameter W_XSIZE = 960,
    parameter W_YSIZE = 540
)(
    input ddrphy_clkin,
    input ddr_init_done,
    input cam_pclk,
    input cmos_frame_vsync,
    input cmos_frame_valid,
    input [31:0] wr_data_camera,
    input [7:0] bufn_i,

    input video_vsync,
    input video_valid,
    output [31:0] rd_data_video,
    input rbufi,


    output axi_wstart_locked,
    output [3:0] M_AWID,
    output [31:0] M_AWADDR,
    output [7:0] M_AWLEN,
    output M_AWVALID,
    input M_AWREADY,
    output [255:0] M_WDATA,
    output [31:0] M_WSTRB,
    output M_WLAST,
    input M_WREADY,
    output [3:0] M_ARID,
    output [31:0] M_ARADDR,
    output [7:0] M_ARLEN,
    output M_ARVALID,
    input M_ARREADY,
    input [3:0] M_RID,
    input [255:0] M_RDATA,
    output M_RLAST,
    input M_RVALID
);

    wire [31:0] dma_waddr;
    wire dma_wareq;
    wire [15:0] dma_wsize;
    wire dma_wbusy;
    wire [255:0] dma_wdata;
    wire dma_wvalid;
    wire dma_wready;
    wire [31:0] dma_raddr;
    wire dma_rareq;
    wire [15:0] dma_rsize;
    wire dma_rbusy;
    wire [255:0] dma_rdata;
    wire dma_rvalid;
    wire dma_rready;

dma #(
    .ENABLE_READ        (ENABLE_READ),
    .W_BASEADDR         (W_BASEADDR),
    .W_XSIZE            (W_XSIZE),
    .W_YSIZE            (W_YSIZE)         
    )
u_dma(
	.ui_clk				(ddrphy_clkin),         
	.ui_rstn			    (ddr_init_done),

	.W_wclk_i			(cam_pclk),       
	.W_FS_i				(cmos_frame_vsync),         
	.W_wren_i			(cmos_frame_valid),       
	.W_data_i			(wr_data_camera),       
	.W_sync_cnt_o 		(),
	.W_buf_i			    (bufn_i),               				
	.dma_waddr			(dma_waddr),     
	.dma_wareq			(dma_wareq),     
	.dma_wsize			(dma_wsize),     
	.dma_wbusy			(dma_wbusy),		
	.dma_wdata			(dma_wdata),     
	.dma_wvalid		    (dma_wvalid),    
	.dma_wready		    (dma_wready),

    .R_rclk_i           (cam_pclk),
    .R_FS_i             (video_vsync),
    .R_rden_i           (video_valid),
    .R_data_o           (rd_data_video),
    .R_sync_cnt_o       (),
    .R_buf_i            (rbufi),
    .R_empty            (),

    .dma_raddr          (dma_raddr),
    .dma_rareq          (dma_rareq),
    .dma_rsize          (dma_rsize),
    .dma_rbusy          (dma_rbusy),
    .dma_rdata          (dma_rdata),
    .dma_rvalid         (dma_rvalid),
    .dma_rready         (dma_rready)
    );

cvt2axi    u_cvt2axi(
    .dma_waddr        (dma_waddr),		
    .dma_wareq        (dma_wareq),
    .dma_wsize        (dma_wsize),
    .dma_wbusy        (dma_wbusy),
    .dma_wdata        (dma_wdata),
    .dma_wvalid       (dma_wvalid),
    .dma_wready       (dma_wready),
    .dma_raddr        (dma_raddr),
    .dma_rareq        (dma_rareq),
    .dma_rsize        (dma_rsize),
    .dma_rbusy        (dma_rbusy),
    .dma_rdata        (dma_rdata),
    .dma_rvalid       (dma_rvalid),
    .dma_rready       (dma_rready),
    .axi_wstart_locked (axi_wstart_locked),
    .M_AXI_ACLK		   (ddrphy_clkin),
    .M_AXI_ARESETN	   (ddr_init_done),
    .M_AXI_AWID		   (M_AWID),
    .M_AXI_AWADDR	   (M_AWADDR),
    .M_AXI_AWLEN       (M_AWLEN),
    .M_AXI_AWVALID     (M_AWVALID),
    .M_AXI_AWREADY     (M_AWREADY),
    .M_AXI_WID         (),
    .M_AXI_WDATA       (M_WDATA),
    .M_AXI_WSTRB       (M_WSTRB),
    .M_AXI_WLAST       (M_WLAST),
    .M_AXI_WVALID      (),
    .M_AXI_WREADY      (M_WREADY),
    .M_AXI_ARID        (M_ARID),
    .M_AXI_ARADDR      (M_ARADDR),
    .M_AXI_ARLEN       (M_ARLEN),
    .M_AXI_ARVALID      (M_ARVALID),
    .M_AXI_ARREADY     (M_ARREADY),
    .M_AXI_RID         (M_RID),
    .M_AXI_RDATA       (M_RDATA),
    .M_AXI_RLAST       (M_RLAST),
    .M_AXI_RVALID      (M_RVALID),
    .M_AXI_RREADY       ()      
);

endmodule