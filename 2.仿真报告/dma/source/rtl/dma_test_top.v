module dma_test_top #(
    parameter ADDR_WIDTH = 32,
    parameter DATA_WIDTH = 256,
    parameter ID_WIDTH = 4
)(
    input ddrphy_clkin,
    input ddr_init_done,
    input cam_pclk,
//    master0    
    input cmos_frame_vsync0,
    input cmos_frame_valid0,
    input [31:0] wr_data_camera0,
    input [7:0] bufn_i0,
//    master1
    input cmos_frame_vsync1,
    input cmos_frame_valid1,
    input [31:0] wr_data_camera1,
    input [7:0] bufn_i1,
//    master2    
    input cmos_frame_vsync2,
    input cmos_frame_valid2,
    input [31:0] wr_data_camera2,
    input [7:0] bufn_i2,
//    master3    
    input cmos_frame_vsync3,
    input cmos_frame_valid3,
    input [31:0] wr_data_camera3,
    input [7:0] bufn_i3,

    output [ADDR_WIDTH-1:0] axi_awaddr,    
    output axi_awuser_ap, 
    output [ID_WIDTH-1:0] axi_awuser_id, 
    output [3:0] axi_awlen,     
    input axi_awready,   
    output axi_awvalid,   
               
    output [DATA_WIDTH-1:0] axi_wdata,     
    output [31:0] axi_wstrb,     
    input axi_wready,    
    input axi_wusero_id, 
    input axi_wusero_last,
               
    output [ADDR_WIDTH-1:0] axi_araddr,    
    output axi_aruser_ap, 
    output [ID_WIDTH-1:0] axi_aruser_id, 
    output [3:0] axi_arlen,     
    input axi_arready,   
    output axi_arvalid,   
               
    input [DATA_WIDTH-1:0] axi_rdata,     
    input [ID_WIDTH-1:0]axi_rid,       
    input axi_rlast,     
    input axi_rvalid     
   );

    wire [3:0] s0_awid;
    wire [31:0] s0_awaddr;
    wire [7:0] s0_awlen;
    wire s0_awvalid;
    wire s0_awready;
    wire [255:0] s0_wdata;
    wire [31:0] s0_wstrb;
    wire s0_wlast;
    wire s0_wready;
    wire axi_wstart_locked0;

    wire [3:0] s0_arid;
    wire [31:0] s0_araddr;
    wire [7:0] s0_arlen;
    wire s0_arvalid;
    wire s0_arready;
    wire s0_rvalid;
    wire [255:0] s0_rdata;
    wire s0_rlast;
 
    wire [3:0] s1_awid;
    wire [31:0] s1_awaddr;
    wire [7:0] s1_awlen;
    wire s1_awvalid;
    wire s1_awready;
    wire [255:0] s1_wdata;
    wire [31:0] s1_wstrb;
    wire s1_wlast;
    wire s1_wready;
    wire axi_wstart_locked1;

    wire [3:0] s2_awid;
    wire [31:0] s2_awaddr;
    wire [7:0] s2_awlen;
    wire s2_awvalid;
    wire s2_awready;
    wire [255:0] s2_wdata;
    wire [31:0] s2_wstrb;
    wire s2_wlast;
    wire s2_wready;
    wire axi_wstart_locked2;

    wire [3:0] s3_awid;
    wire [31:0] s3_awaddr;
    wire [7:0] s3_awlen;
    wire s3_awvalid;
    wire s3_awready;
    wire [255:0] s3_wdata;
    wire [31:0] s3_wstrb;
    wire s3_wlast;
    wire s3_wready;
    wire axi_wstart_locked3;

vsdma #(
    .ENABLE_READ(1'b1),
    .W_BASEADDR(32'd0),
    .W_XSIZE(960),
    .W_YSIZE(540)        
)    
u0_vsdma(
    .ddrphy_clkin(ddrphy_clkin),
    .ddr_init_done(ddr_init_done),
    .cam_pclk(cam_pclk),
    .cmos_frame_vsync(cmos_frame_vsync0),
    .cmos_frame_valid(cmos_frame_valid0),
    .wr_data_camera(wr_data_camera0),
    .bufn_i(bufn_i0),
    .video_vsync(),
    .video_valid(),
    .rd_data_video(),
    .rbufi(),
    .axi_wstart_locked(axi_wstart_locked0),
    .M_AWID(s0_awid),
    .M_AWADDR(s0_awaddr),
    .M_AWLEN(s0_awlen),
    .M_AWVALID(s0_awvalid),
    .M_AWREADY(s0_awready),
    .M_WDATA(s0_wdata),
    .M_WSTRB(s0_wstrb),
    .M_WLAST(s0_wlast),
    .M_WREADY(s0_wready),
    .M_ARID(s0_arid),
    .M_ARADDR(s0_araddr),
    .M_ARLEN(s0_arlen),
    .M_ARVALID(s0_arvalid),
    .M_ARREADY(s0_arready),
    .M_RID(),
    .M_RDATA(s0_rdata),
    .M_RLAST(s0_rlast),
    .M_RVALID(s0_rvalid)
);

vsdma #(
    .ENABLE_READ(1'b0),
    .W_BASEADDR(32'd960),
    .W_XSIZE(960),
    .W_YSIZE(540)        
)    
u1_vsdma(
    .ddrphy_clkin(ddrphy_clkin),
    .ddr_init_done(ddr_init_done),
    .cam_pclk(cam_pclk),
    .cmos_frame_vsync(cmos_frame_vsync1),
    .cmos_frame_valid(cmos_frame_valid1),
    .wr_data_camera(wr_data_camera1),
    .bufn_i(bufn_i1),
    .axi_wstart_locked(axi_wstart_locked1),
    .M_AWID(s1_awid),
    .M_AWADDR(s1_awaddr),
    .M_AWLEN(s1_awlen),
    .M_AWVALID(s1_awvalid),
    .M_AWREADY(s1_awready),
    .M_WDATA(s1_wdata),
    .M_WSTRB(s1_wstrb),
    .M_WLAST(s1_wlast),
    .M_WREADY(s1_wready)
);

vsdma #(
    .ENABLE_READ(1'b0),
    .W_BASEADDR(32'd1036800),
    .W_XSIZE(960),
    .W_YSIZE(540)        
)    
u2_vsdma(
    .ddrphy_clkin(ddrphy_clkin),
    .ddr_init_done(ddr_init_done),
    .cam_pclk(cam_pclk),
    .cmos_frame_vsync(cmos_frame_vsync2),
    .cmos_frame_valid(cmos_frame_valid2),
    .wr_data_camera(wr_data_camera2),
    .bufn_i(bufn_i2),
    .axi_wstart_locked(axi_wstart_locked2),
    .M_AWID(s2_awid),
    .M_AWADDR(s2_awaddr),
    .M_AWLEN(s2_awlen),
    .M_AWVALID(s2_awvalid),
    .M_AWREADY(s2_awready),
    .M_WDATA(s2_wdata),
    .M_WSTRB(s2_wstrb),
    .M_WLAST(s2_wlast),
    .M_WREADY(s2_wready)
);

vsdma #(
    .ENABLE_READ(1'b0),
    .W_BASEADDR(32'd1037760),
    .W_XSIZE(960),
    .W_YSIZE(540)        
)    
u3_vsdma(
    .ddrphy_clkin(ddrphy_clkin),
    .ddr_init_done(ddr_init_done),
    .cam_pclk(cam_pclk),
    .cmos_frame_vsync(cmos_frame_vsync3),
    .cmos_frame_valid(cmos_frame_valid3),
    .wr_data_camera(wr_data_camera3),
    .bufn_i(bufn_i3),
    .axi_wstart_locked(axi_wstart_locked3),
    .M_AWID(s3_awid),
    .M_AWADDR(s3_awaddr),
    .M_AWLEN(s3_awlen),
    .M_AWVALID(s3_awvalid),
    .M_AWREADY(s3_awready),
    .M_WDATA(s3_wdata),
    .M_WSTRB(s3_wstrb),
    .M_WLAST(s3_wlast),
    .M_WREADY(s3_wready)
);

AXI_Interconnect #(
    .DATA_WIDTH(256),
    .ADDR_WIDTH(32),
    .ID_WIDTH(4)
)    
u_AXI_Interconnect(
	.ACLK(ddrphy_clkin),
	.ARESETn(ddr_init_done),

	.s0_AWID(s0_awid),
    .s0_AWADDR(s0_awaddr),
    .s0_AWLEN(s0_awlen),
    .s0_AWVALID(s0_awvalid),
    .s0_AWREADY(s0_awready),
 
    .s0_WDATA(s0_wdata),
    .s0_WSTRB(s0_wstrb),
    .s0_WLAST(s0_wlast),
    .s0_WREADY(s0_wready),

    .axi_wstart_locked0(axi_wstart_locked0),

    .s0_ARID(s0_arid),
    .s0_ARADDR(s0_araddr),
    .s0_ARLEN(s0_arlen),
    .s0_ARVALID(s0_arvalid),
    .s0_ARREADY(s0_arready),

    .s0_RVALID(s0_rvalid),
	.s0_RDATA(s0_rdata),
    .s0_RLAST(s0_rlast),

	.s1_AWID(s1_awid),
    .s1_AWADDR(s1_awaddr),
    .s1_AWLEN(s1_awlen),
    .s1_AWVALID(s1_awvalid),
    .s1_AWREADY(s1_awready),

    .axi_wstart_locked1(axi_wstart_locked1),

    .s1_WDATA(s1_wdata),
    .s1_WSTRB(s1_wstrb),
    .s1_WLAST(s1_wlast),
    .s1_WREADY(s1_wready),
 
	.s2_AWID(s2_awid),
    .s2_AWADDR(s2_awaddr),
    .s2_AWLEN(s2_awlen),
    .s2_AWVALID(s2_awvalid),
    .s2_AWREADY(s2_awready),
    
    .axi_wstart_locked2(axi_wstart_locked2),
        
    .s2_WDATA(s2_wdata),
    .s2_WSTRB(s2_wstrb),
    .s2_WLAST(s2_wlast),
    .s2_WREADY(s2_wready),
  
	.s3_AWID(s3_awid),
    .s3_AWADDR(s3_awaddr),
    .s3_AWLEN(s3_awlen),
    .s3_AWVALID(s3_awvalid),
    .s3_AWREADY(s3_awready),
    
    .axi_wstart_locked3(axi_wstart_locked3),
           
    .s3_WDATA(s3_wdata),
    .s3_WSTRB(s3_wstrb),
    .s3_WLAST(s3_wlast),
    .s3_WREADY(s3_wready),
    
    .axi_awaddr(axi_awaddr),
    .axi_awuser_ap(axi_awuser_ap),
    .axi_awuser_id(axi_awuser_id),
    .axi_awlen(axi_awlen),
    .axi_awready(axi_awready),
    .axi_awvalid(axi_awvalid),
    
    .axi_wdata(axi_wdata),
    .axi_wstrb(axi_wstrb),
    .axi_wready(axi_wready),
    .axi_wusero_id(axi_wusero_id),
    .axi_wusero_last(axi_wusero_last),
    
    .axi_araddr(axi_araddr),
    .axi_aruser_ap(axi_aruser_ap),
    .axi_aruser_id(axi_aruser_id),
    .axi_arlen(axi_arlen),
    .axi_arready(axi_arready),
    .axi_arvalid(axi_arvalid),
    
    .axi_rdata(axi_rdata),
    .axi_rid(axi_rid),
    .axi_rlast(axi_rlast),
    .axi_rvalid(axi_rvalid)
);
endmodule