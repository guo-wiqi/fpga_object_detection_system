module dma#(
parameter  integer                   VIDEO_ENABLE   = 1,  
parameter  integer                   ENABLE_WRITE   = 1,
parameter  integer                   ENABLE_READ    = 0,

parameter  integer                   AXI_DATA_WIDTH = 256,
parameter  integer                   AXI_ADDR_WIDTH = 28,

parameter  integer                   W_BUFDEPTH     = 2048,
parameter  integer                   W_DATAWIDTH    = 32,
parameter  [AXI_ADDR_WIDTH -1'b1: 0] W_BASEADDR     = 0,
parameter  integer                   W_DSIZEBITS    = 22,
parameter  integer                   W_XSIZE        = 960, 
parameter  integer                   W_XSTRIDE      = 1920,
parameter  integer                   W_YSIZE        = 540,
parameter  integer                   W_XDIV         = 2,
parameter  integer                   W_BUFSIZE      = 3,

parameter  integer                   R_BUFDEPTH     = 2048,
parameter  integer                   R_DATAWIDTH    = 32,
parameter  [AXI_ADDR_WIDTH -1'b1: 0] R_BASEADDR     = 0,
parameter  integer                   R_DSIZEBITS    = 22,
parameter  integer                   R_XSIZE        = 1920, 
parameter  integer                   R_XSTRIDE      = 1920,
parameter  integer                   R_YSIZE        = 1080,
parameter  integer                   R_XDIV         = 2,
parameter  integer                   R_BUFSIZE      = 3
)
(
input wire                                  ui_clk,
input wire                                  ui_rstn,
// input -W_FIFO--------------
input wire                                  W_wclk_i,
input wire                                  W_FS_i,
input wire                                  W_wren_i,
input wire     [W_DATAWIDTH-1'b1 : 0]       W_data_i,
output reg     [7   :0]                     W_sync_cnt_o =0, 
input  wire    [7   :0]                     W_buf_i,
output wire                                 W_full,
//----------dma signals write-------       
output wire    [AXI_ADDR_WIDTH-1'b1: 0]     dma_waddr,
output wire                                 dma_wareq,
output wire    [15  :0]                     dma_wsize,                                     
input  wire                                 dma_wbusy,		
output wire    [AXI_DATA_WIDTH-1'b1:0]      dma_wdata,
input  wire                                 dma_wvalid,
output wire                                 dma_wready,		
 // input -R_FIFO--------------
input  wire                                 R_rclk_i,
input  wire                                 R_FS_i,
input  wire                                 R_rden_i,
output wire    [R_DATAWIDTH-1'b1 : 0]       R_data_o,
output reg     [7   :0]                     R_sync_cnt_o =0,
input  wire    [7   :0]                     R_buf_i,
output wire                                 R_empty,
//----------dma signals read------- 
output wire    [AXI_ADDR_WIDTH-1'b1: 0]     dma_raddr,
output wire                                 dma_rareq,
output wire    [15: 0]                      dma_rsize,                                     
input  wire                                 dma_rbusy,			
input  wire    [AXI_DATA_WIDTH-1'b1:0]      dma_rdata,
input  wire                                 dma_rvalid,
output wire                                 dma_rready
); 

function integer clog2;
  input integer value;
  begin 
    value = value-1;
    for (clog2=0; value>0; clog2=clog2+1)
      value = value>>1;
    end 
  endfunction
  
localparam S_IDLE  =  2'd0;  
localparam S_RST   =  2'd1;  
localparam S_DATA1 =  2'd2;   
localparam S_DATA2 =  2'd3; 

generate  if(ENABLE_WRITE == 1)begin : WRITE_ENABLE

localparam WFIFO_DEPTH = W_BUFDEPTH;
localparam W_WR_DATA_COUNT_WIDTH = clog2(WFIFO_DEPTH)+1;
localparam W_RD_DATA_COUNT_WIDTH = clog2(WFIFO_DEPTH*W_DATAWIDTH/AXI_DATA_WIDTH)+1;

localparam WYBUF_SIZE           = (W_BUFSIZE - 1'b1);
localparam WY_BURST_TIMES       = (W_YSIZE*W_XDIV);
localparam FDMA_WX_BURST        = (W_XSIZE*W_DATAWIDTH/AXI_DATA_WIDTH)/W_XDIV;  //burst 1 line 
localparam WX_BURST_ADDR_INC    = (W_XSIZE*(W_DATAWIDTH/32))/W_XDIV;
localparam WX_LAST_ADDR_INC     = (W_XSTRIDE-W_XSIZE)*(W_DATAWIDTH/32) + WX_BURST_ADDR_INC;

assign                                  dma_wready = 1'b1;
reg                                     dma_wareq_r= 1'b0;
reg                                     W_FIFO_Rst=0; 
wire                                    W_FS;
reg [1 :0]                              W_MS=0; 
reg [W_DSIZEBITS-1'b1:0]                W_addr=0; 
reg [15:0]                              W_bcnt=0; //write burst cnt
wire[W_RD_DATA_COUNT_WIDTH-1'b1 :0]     W_rcnt;
reg                                     W_REQ=0; 
reg [3 :0]                              wdiv_cnt =0;
reg [7 :0]                              wrst_cnt =0;
reg [7 :0]                              fmda_wbufn;

assign dma_wsize = FDMA_WX_BURST;
assign dma_waddr = W_BASEADDR + {fmda_wbufn,W_addr};

reg [1:0] W_MS_r =0;

always @(posedge ui_clk) 
	W_MS_r <= W_MS;
	
fs_cap #
(
.VIDEO_ENABLE(VIDEO_ENABLE)
)
fs_cap_W0
(
 .clk_i(ui_clk),
 .rstn_i(ui_rstn),
 .vs_i(W_FS_i),
 .fs_cap_o(W_FS)
);

assign dma_wareq = dma_wareq_r;
reg pre_read_cnt,pre_read_cnt_d;
reg pre_read;
always@(posedge ui_clk) begin
    if(W_FS)
        pre_read_cnt <= 1'b0;
    else if(dma_wareq && dma_wbusy)
        pre_read_cnt <= pre_read_cnt + 1'b1;
    else
        pre_read_cnt <= pre_read_cnt;
end
always@(posedge ui_clk) begin
    pre_read_cnt_d <= pre_read_cnt;
end
always@(posedge ui_clk) begin
    if(W_FS)
        pre_read <= 1'b0;
    else if(~pre_read_cnt_d && pre_read_cnt)
        pre_read <= 1'b1;
    else
        pre_read <= 1'b0;
end


always @(posedge ui_clk) begin
   if(!ui_rstn)begin
       W_MS         <= S_IDLE;
       W_FIFO_Rst   <= 0     ;
       W_addr       <= 0     ;
       W_sync_cnt_o <= 0     ;
       W_bcnt       <= 0     ;
       wrst_cnt     <= 0     ;
       wdiv_cnt     <= 0     ;
       fmda_wbufn   <= 0     ;
       dma_wareq_r <= 1'd0  ;
   end   
   else begin
     case(W_MS)
       S_IDLE:begin
         W_addr <= 0;
         W_bcnt <= 0;
         wrst_cnt <= 0;
         wdiv_cnt <=0;
         if(W_FS) begin //for video is vs ; for other data is reset
           W_MS <= S_RST;
           if(W_sync_cnt_o < WYBUF_SIZE) W_sync_cnt_o <= W_sync_cnt_o + 1'b1; 
           else W_sync_cnt_o <= 0;  
         end
       end
       S_RST:begin //fifo reset must do it
          fmda_wbufn <= W_buf_i;
          wrst_cnt <= wrst_cnt + 1'b1;
          if((VIDEO_ENABLE == 1) && (wrst_cnt < 15)) W_FIFO_Rst <= 1;
          else if((VIDEO_ENABLE == 1) && (wrst_cnt == 15)) W_FIFO_Rst <= 0;
          else W_MS <= S_DATA1;
       end
       S_DATA1:begin 
         if(dma_wbusy == 1'b0 && W_REQ ) dma_wareq_r  <= 1'b1; 
         else if(dma_wbusy == 1'b1) begin
            dma_wareq_r  <= 1'b0;
            W_MS    <= S_DATA2;
         end          
       end
       S_DATA2:begin 
           if(dma_wbusy == 1'b0) begin //1 burst ok=1/2 line ok
               if(W_bcnt == WY_BURST_TIMES - 1'b1) W_MS <= S_IDLE; //1 frame burst ok
               else begin
                   W_bcnt <= W_bcnt + 1'b1;
                   W_MS    <= S_DATA1;               
                   if(wdiv_cnt < W_XDIV - 1'b1)begin
                       W_addr <= W_addr +  WX_BURST_ADDR_INC;  
                       wdiv_cnt <= wdiv_cnt + 1'b1;
                   end
                   else begin //1 line burst ok
                       W_addr <= W_addr + WX_LAST_ADDR_INC;
                       wdiv_cnt <= 0;
                   end
               end 
           end
       end
        default: W_MS <= S_IDLE; 
      endcase
   end
end 

always@(posedge ui_clk) W_REQ  <= (W_rcnt > FDMA_WX_BURST - 2)&&((ui_rstn == 1'b1) || (W_FIFO_Rst == 1'b0)); 

fifo_in u_fifoin (                             
 .wr_clk(W_wclk_i),                    // input         
 .wr_rst((ui_rstn == 1'b0) || (W_FIFO_Rst == 1'b1)),                    // input         
 .wr_en(W_wren_i),                      // input         
 .wr_data(W_data_i),                  // input [31:0]  
 .wr_full(W_full),                  // output        
 .wr_water_level(),    // output [12:0] 
 .almost_full(),          // output        
 .rd_clk(ui_clk),                    // input         
 .rd_rst((ui_rstn == 1'b0) || (W_FIFO_Rst == 1'b1)),                    // input         
 .rd_en(dma_wvalid||pre_read),                      // input         
 .rd_data(dma_wdata),                  // output [127:0]
 .rd_empty(),                // output        
 .rd_water_level(W_rcnt),    // output [10:0] 
 .almost_empty()         // output        
); 

end
else begin : FDMA_WRITE_DISABLE

//----------dma signals write-------       
assign dma_waddr = 0;
assign dma_wareq = 0;
assign dma_wsize = 0;                                   	
assign dma_wdata = 0;
assign dma_wready = 0;
assign dma_wirq = 0;	
assign W_full = 0;

end
endgenerate


generate  if(ENABLE_READ == 1)begin : FDMA_READ

localparam RFIFO_DEPTH = R_BUFDEPTH*R_DATAWIDTH/AXI_DATA_WIDTH;
localparam R_WR_DATA_COUNT_WIDTH = clog2(RFIFO_DEPTH)+1;
localparam R_RD_DATA_COUNT_WIDTH = clog2(R_BUFDEPTH)+1;

localparam RYBUF_SIZE           = (R_BUFSIZE - 1'b1);
localparam RY_BURST_TIMES       = (R_YSIZE*R_XDIV);
localparam FDMA_RX_BURST        = (R_XSIZE*R_DATAWIDTH/AXI_DATA_WIDTH)/R_XDIV;
localparam RX_BURST_ADDR_INC    = (R_XSIZE*(R_DATAWIDTH/32))/R_XDIV;
localparam RX_LAST_ADDR_INC     = (R_XSTRIDE-R_XSIZE)*(R_DATAWIDTH/32) + RX_BURST_ADDR_INC;

assign                                  dma_rready = 1'b1;
reg                                     dma_rareq_r= 1'b0;
reg                                     R_FIFO_Rst=0; 
wire                                    R_FS;
reg [1 :0]                              R_MS=0; 
reg [R_DSIZEBITS-1'b1:0]                R_addr=0; 
reg [15:0]                              R_bcnt=0; 
wire[R_WR_DATA_COUNT_WIDTH-1'b1 :0]     R_wcnt;
reg                                     R_REQ=0; 
reg [3 :0]                              rdiv_cnt =0;
reg [7 :0]                              rrst_cnt =0;
reg [7 :0]                              fmda_rbufn;

assign dma_rsize = FDMA_RX_BURST;
assign dma_raddr = R_BASEADDR + {fmda_rbufn,R_addr};

reg [1:0] R_MS_r =0;
always @(posedge ui_clk) 
	R_MS_r <= R_MS;

fs_cap #
(
.VIDEO_ENABLE(VIDEO_ENABLE)
)
fs_cap_R0
(
  .clk_i(ui_clk),
  .rstn_i(ui_rstn),
  .vs_i(R_FS_i),
  .fs_cap_o(R_FS)
);

assign dma_rareq = dma_rareq_r;

 always @(posedge ui_clk) begin
   if(!ui_rstn)begin
        R_MS          <= S_IDLE;
        R_FIFO_Rst   <= 0;
        R_addr       <= 0;
        R_sync_cnt_o <= 0;
        R_bcnt       <= 0;
        rrst_cnt     <= 0;
        rdiv_cnt      <= 0;
        fmda_rbufn    <= 0;
        dma_rareq_r  <= 1'd0;
    end   
    else begin
      case(R_MS)
        S_IDLE:begin
          R_addr <= 0;
          R_bcnt <= 0;
          rrst_cnt <= 0;
          rdiv_cnt <=0;
          if(R_FS) begin
            R_MS <= S_RST;
            if(R_sync_cnt_o < RYBUF_SIZE) R_sync_cnt_o <= R_sync_cnt_o + 1'b1; 
            else R_sync_cnt_o <= 0;  
          end
       end
       S_RST:begin
           fmda_rbufn <= R_buf_i;
           rrst_cnt <= rrst_cnt + 1'b1;
           if((VIDEO_ENABLE == 1) && (rrst_cnt < 15)) R_FIFO_Rst <= 1;
           else if((VIDEO_ENABLE == 1) && (rrst_cnt == 15)) R_FIFO_Rst <= 0;
           else R_MS <= S_DATA1;
       end
       S_DATA1:begin 
         if(dma_rbusy == 1'b0 && R_REQ) dma_rareq_r  <= 1'b1;  
         else if(dma_rbusy == 1'b1) begin
            dma_rareq_r  <= 1'b0;
            R_MS    <= S_DATA2;
         end         
        end
        S_DATA2:begin 
            if(dma_rbusy == 1'b0)begin
                if(R_bcnt == RY_BURST_TIMES - 1'b1) R_MS <= S_IDLE;
                else begin
                    R_bcnt <= R_bcnt + 1'b1;
                    R_MS    <= S_DATA1;
                    if(rdiv_cnt < R_XDIV - 1'b1)begin
                        R_addr <= R_addr +  RX_BURST_ADDR_INC;  
                        rdiv_cnt <= rdiv_cnt + 1'b1;
                     end
                    else begin
                        R_addr <= R_addr + RX_LAST_ADDR_INC;
                        rdiv_cnt <= 0;
                    end

                end 
            end
         end
         default:R_MS <= S_IDLE;
      endcase
   end
end 

always@(posedge ui_clk) R_REQ  <= (R_wcnt < FDMA_RX_BURST - 2)&&((ui_rstn == 1'b1) || (R_FIFO_Rst == 1'b0));

fifo_out u_fifoout (
  .wr_clk(ui_clk),                    // input
  .wr_rst((ui_rstn == 1'b0) || (R_FIFO_Rst == 1'b1)),                    // input
  .wr_en(dma_rvalid),                      // input
  .wr_data(dma_rdata),                  // input [127:0]
  .wr_full(),                  // output
  .wr_water_level(R_wcnt),    // output [10:0]
  .almost_full(),          // output
  .rd_clk(R_rclk_i),                    // input
  .rd_rst((ui_rstn == 1'b0) || (R_FIFO_Rst == 1'b1)),                    // input
  .rd_en(R_rden_i),                      // input
  .rd_data(R_data_o),                  // output [31:0]
  .rd_empty(R_empty),                // output
  .rd_water_level(),    // output [12:0]
  .almost_empty()         // output
);

end
else begin : FDMA_READ_DISABLE
   
assign dma_raddr = 0;
assign dma_rareq = 0;
assign dma_rsize = 0;                                   	
assign dma_rdata = 0;
assign dma_rready = 0;
assign dma_rirq = 0;	
assign R_empty = 1'b0;
assign R_data_o =0;
end
endgenerate

endmodule